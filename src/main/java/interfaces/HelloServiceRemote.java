package interfaces;

import javax.ejb.Remote;

@Remote
public interface HelloServiceRemote {
	String SayHello(String msg);
}
