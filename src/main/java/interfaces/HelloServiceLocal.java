package interfaces;

import javax.ejb.Local;

@Local
public interface HelloServiceLocal {
	String SayHello(String msg);
}
